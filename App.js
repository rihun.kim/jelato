import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {persistCache} from 'apollo-cache-persist';
import {onError} from 'apollo-link-error';
import ApolloClient from 'apollo-client';
import {ApolloProvider} from 'react-apollo-hooks';
import {HttpLink} from 'apollo-link-http';
import {ApolloLink, split} from 'apollo-link';
import {WebSocketLink} from 'apollo-link-ws';
import {getMainDefinition} from 'apollo-utilities';

import {AuthProvider} from './src/Apollo/ClientState';
import Navigation from './src/Components/Navigation/Container/Main';

const App = () => {
  console.log('DEV:: App');

  const [apolloClient, setApolloClient] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  const preload = async () => {
    try {
      const cache = new InMemoryCache();
      await persistCache({cache, storage: AsyncStorage});

      const token = await AsyncStorage.getItem('TOKEN');
      const httpLink = new HttpLink({
        uri:
          process.env.NODE_ENV === 'development'
            ? 'http://192.168.0.111:1990/'
            : // 'http://192.168.35.2:1990/'
              'NOTYET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const wsLink = new WebSocketLink({
        uri:
          process.env.NODE_ENV === 'development'
            ? `ws://192.168.0.111:1990`
            : // 'ws://192.168.35.2:1990/'
              'NOTYET',
        options: {
          reconnect: true,
          connectionParams: {
            token: token,
          },
        },
      });

      setApolloClient(
        new ApolloClient({
          cache: cache,
          link: ApolloLink.from([
            onError(({graphQLErrors, networkError}) => {
              if (graphQLErrors)
                graphQLErrors.map(({message, locations, path}) =>
                  console.log(
                    `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                  ),
                );
              if (networkError) console.log(`[Network error]: ${networkError}`);
            }),
            split(
              ({query}) => {
                const definition = getMainDefinition(query);
                return (
                  definition.kind === 'OperationDefinition' &&
                  definition.operation === 'subscription'
                );
              },
              wsLink,
              httpLink,
            ),
          ]),
        }),
      );

      const isLogin = await AsyncStorage.getItem('isLogin');
      if (!isLogin || isLogin === 'false') setIsLogin(false);
      else setIsLogin(true);

      SplashScreen.hide();
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    preload();
  }, []);

  if (!apolloClient) return <></>;
  return (
    <ApolloProvider client={apolloClient}>
      <AuthProvider isLogin={isLogin}>
        <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />
        <Navigation />
      </AuthProvider>
    </ApolloProvider>
  );
};

export default App;

{
  /* <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={{flex: 1}}></KeyboardAvoidingView> */
}
