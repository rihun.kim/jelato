import React from 'react';
import { ApolloProvider } from 'react-apollo-hooks';

import { AuthProvider } from '../Auth/Context';
import Navigation from '../Components/Navigation/Container/Main';

export const ExecRoute = ({ apolloClient, userToken }) => {
  console.log('DEV:: Exec');

  return (
    <ApolloProvider client={apolloClient}>
      <AuthProvider userToken={userToken}>
        <Navigation />
      </AuthProvider>
    </ApolloProvider>
  );
};

export const WaitRoute = () => {
  console.log('DEV:: Wait');
  return <></>;
};
