import ApolloClient from 'apollo-client';
import { getMainDefinition } from 'apollo-utilities';
import { ApolloLink, split } from 'apollo-link';
import { onError } from 'apollo-link-error';

import { setCache } from './Cache';
import { authHttpLink, authWsLink } from './Link';
import { handleGraphqlErrors, handleNetworkError } from './Error';

export const setApollo = async setApolloClient => {
  const cache = await setCache();

  setApolloClient(
    new ApolloClient({
      cache: cache,
      link: ApolloLink.from([
        onError(({ graphQLErrors, networkError }) => {
          if (graphQLErrors) handleGraphqlErrors(graphQLErrors);
          if (networkError) handleNetworkError(networkError);
        }),
        split(
          ({ query }) => {
            const definition = getMainDefinition(query);
            return (
              definition.kind == 'OperationDefinition' &&
              definition.operation == 'subscription'
            );
          },
          authWsLink,
          authHttpLink,
        ),
      ]),
    }),
  );
};
