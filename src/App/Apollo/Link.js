import AsyncStorage from '@react-native-community/async-storage';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { setContext } from 'apollo-link-context';

import {
  DEV_HTTP_SERVER_IP,
  OPS_HTTP_SERVER_IP,
  DEV_WS_SERVER_IP,
  OPS_WS_SERVER_IP,
  ASYNC_KEY_USERTOKEN,
} from './Constant';

//
const httpLink = new HttpLink({
  uri:
    process.env.NODE_ENV == 'development'
      ? DEV_HTTP_SERVER_IP
      : OPS_HTTP_SERVER_IP,
});

const httpMiddle = setContext(async (_, { headers }) => {
  const token = await AsyncStorage.getItem(ASYNC_KEY_USERTOKEN);
  return {
    headers: {
      ...headers,
      Authorization: `Bearer ${token}`,
    },
  };
});

export const authHttpLink = httpMiddle.concat(httpLink);

//
const wsLink = new WebSocketLink({
  uri:
    process.env.NODE_ENV == 'development' ? DEV_WS_SERVER_IP : OPS_WS_SERVER_IP,
  options: {
    lazy: true,
    reconnect: true,
    connectionParams: async () => {
      const token = await AsyncStorage.getItem(ASYNC_KEY_USERTOKEN);
      return {
        token: token,
      };
    },
  },
});

export const authWsLink = wsLink;
