import { InMemoryCache } from '@apollo/client';
import { persistCache } from 'apollo-cache-persist';
import AsyncStorage from '@react-native-community/async-storage';

export const setCache = async () => {
  const cache = new InMemoryCache();
  await persistCache({ cache, storage: AsyncStorage });

  return cache;
};
