export const handleGraphqlErrors = graphQLErrors => {
  graphQLErrors.map(({ message, locations, path }) =>
    console.log(
      `GraphQL Error:: Message: ${message}, Location: ${locations}, Path: ${path}`,
    ),
  );
};

export const handleNetworkError = networkError => {
  const log = JSON.stringify(networkError);
  console.log(`Network Error:: Message: ${log}`);
};
