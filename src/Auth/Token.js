import AsyncStorage from '@react-native-community/async-storage';

import { ASYNC_KEY_USERTOKEN, ASYNC_VALUE_NONE } from './Constant';

export const setAuth = async setUserToken => {
  const userToken = await AsyncStorage.getItem(ASYNC_KEY_USERTOKEN);
  setUserToken(userToken == null ? ASYNC_VALUE_NONE : userToken);
};

export const getUserToken = async () => {
  return await AsyncStorage.getItem(ASYNC_KEY_USERTOKEN);
};

export const setUserToken = async userToken => {
  await AsyncStorage.setItem(ASYNC_KEY_USERTOKEN, userToken);
};

export const clearUserToken = async () => {
  await AsyncStorage.setItem(ASYNC_KEY_USERTOKEN, ASYNC_VALUE_NONE);
};
