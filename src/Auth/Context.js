import React, { createContext, useContext, useState } from 'react';

import { clearUserToken, setUserToken } from './Token';
import { ASYNC_VALUE_NONE } from './Constant';

export const AuthContext = createContext();
export const AuthProvider = ({ userToken, children }) => {
  const [_isLogin, _setIsLogin] = useState(
    userToken == ASYNC_VALUE_NONE ? false : true,
  );

  const _doLogin = async userToken => {
    try {
      await setUserToken(userToken);
      _setIsLogin(true);
    } catch (e) {
      console.log(e);
    }
  };
  const _doLogout = async () => {
    try {
      await clearUserToken();
      _setIsLogin(false);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <AuthContext.Provider value={{ _isLogin, _doLogin, _doLogout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const isLogin = () => {
  const { _isLogin } = useContext(AuthContext);
  return _isLogin;
};

export const useDoLogin = () => {
  const { _doLogin } = useContext(AuthContext);
  return _doLogin;
};

export const useDoLogout = () => {
  const { _doLogout } = useContext(AuthContext);
  return _doLogout;
};
