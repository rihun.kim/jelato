import { gql } from 'apollo-boost';

export const REQUEST_USERTOKEN = gql`
  mutation requestUserToken($email: String!, $password: String!) {
    requestUserToken(email: $email, password: $password)
  }
`;
