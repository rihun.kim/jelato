import React, { useState } from 'react';
import { useMutation } from 'react-apollo-hooks';

import { useDoLogin } from '../../../../Auth/Context';
import { useInput, EmailInput, PasswordInput } from './Input';
import { SendButton, onSend } from './Button';
import { REQUEST_USERTOKEN } from '../Share/Gql';
import { Viewer } from './Resource/Style/StMain';

const Main = () => {
  const [buttonAble, setButtonAble] = useState(true);
  const emailUseInput = useInput('');
  const passwordUseInput = useInput('');
  const doLogin = useDoLogin();
  const [requestUserTokenMutation] = useMutation(REQUEST_USERTOKEN);

  const onSendClick = () => {
    onSend({
      emailUseInput,
      passwordUseInput,
      requestUserTokenMutation,
      doLogin,
      setButtonAble,
    });
  };

  return (
    <Viewer>
      <EmailInput {...emailUseInput} />
      <PasswordInput {...passwordUseInput} />
      <SendButton buttonAble={buttonAble} onClick={onSendClick} />
    </Viewer>
  );
};

export default Main;
