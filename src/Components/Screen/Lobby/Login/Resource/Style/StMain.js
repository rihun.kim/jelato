import styled from 'styled-components';

//
export const Viewer = styled.View`
  width: 100%;
  height: 100%;

  align-items: center;
  justify-content: center;

  background-color: #2b2b28;
`;
