import styled from 'styled-components';

export const EmailInputViewer = styled.TextInput`
  width: 250px;
  height: 40px;

  align-items: center;
  justify-content: center;

  border: 0;
  background-color: #fbfbfb;
  border-radius: 2px;
  font-size: 14px;
  margin: 0 0 1px 0;
  padding: 0 15px 0 15px;
  text-align: center;
`;

export const PasswordInputViewer = styled.TextInput`
  width: 250px;
  height: 40px;

  align-items: center;
  justify-content: center;

  border: 0;
  background-color: #fbfbfb;
  border-radius: 2px;
  font-size: 14px;
  margin: 0 0 10px 0;
  padding: 0 15px 0 15px;
  text-align: center;
`;
