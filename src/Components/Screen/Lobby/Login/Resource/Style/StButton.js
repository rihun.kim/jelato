import styled from 'styled-components';

export const SendButtonViewer = styled.View`
  width: 250px;
  height: 40px;

  align-items: center;
  justify-content: center;

  border: 0;
  background-color: #fdf0f6;
  border-radius: 2px;
`;

export const SendButtonText = styled.Text`
  color: #000000;
  font-size: 14px;
`;
