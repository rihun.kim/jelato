import { sha512 } from 'react-native-sha512';

export const encryptPassword = password => {
  return new Promise((resolve, reject) => {
    sha512(password).then(hash => resolve(hash));
  });
};
