import React, { useState } from 'react';

import {
  EmailInputViewer,
  PasswordInputViewer,
} from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState(prefill);

  const onChange = text => {
    setValue(text);
  };

  return { value, onChange };
};

export const EmailInput = ({ value, onChange }) => (
  <EmailInputViewer
    autoCapitalize={'none'}
    autoCorrect={false}
    keyboardType="email-address"
    onChangeText={onChange}
    onEndEditing={() => null}
    placeholder="email"
    returnKeyType="next"
    value={value}
  />
);

export const PasswordInput = ({ value, onChange, onLoginClick }) => (
  <PasswordInputViewer
    autoCapitalize={'none'}
    autoCorrect={false}
    keyboardType="default"
    onChangeText={onChange}
    onEndEditing={onLoginClick}
    placeholder="password"
    returnKeyType="done"
    secureTextEntry={true}
  />
);
