import React from 'react';
import { ActivityIndicator, TouchableOpacity } from 'react-native';

import { encryptPassword } from './Resource/Encrypt';
import { validateEmail } from './Resource/Validate';
import { SendButtonViewer, SendButtonText } from './Resource/Style/StButton';

export const onSend = async ({
  emailUseInput,
  passwordUseInput,
  requestUserTokenMutation,
  doLogin,
  setButtonAble,
}) => {
  try {
    if (emailUseInput.value == '' || !validateEmail(emailUseInput.value))
      alert('유효한 이메일 입력해주세요.');
    else if (passwordUseInput.value == '') alert('비밀번호 입력해주세요.');
    else {
      setButtonAble(false);
      const {
        data: { requestUserToken: userToken },
      } = await requestUserTokenMutation({
        variables: {
          email: emailUseInput.value,
          password: await encryptPassword(passwordUseInput.value),
        },
      });

      if (userToken.includes('[ALERT]')) {
        setButtonAble(true);
        alert(userToken);
      } else doLogin(userToken);
    }
  } catch (error) {
    alert(error);
  }
};

export const SendButton = ({ buttonAble, onClick }) => {
  return (
    <TouchableOpacity disabled={!buttonAble} onPress={onClick}>
      <SendButtonViewer>
        {buttonAble ? (
          <SendButtonText>login</SendButtonText>
        ) : (
          <ActivityIndicator color="#2b2b28" />
        )}
      </SendButtonViewer>
    </TouchableOpacity>
  );
};
