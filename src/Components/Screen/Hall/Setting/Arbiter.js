import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { useDoLogout } from '../../../../Auth/Context';

const Container = () => {
  const doLogout = useDoLogout();

  return (
    <TouchableOpacity onPress={doLogout}>
      <Text>로그아웃 하기</Text>
    </TouchableOpacity>
  );
};

export default Container;
