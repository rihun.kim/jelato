import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { SendButtonViewer } from './Resource/Style/StButton';

export const SendButton = ({ onClick }) => {
  return (
    <TouchableOpacity onPress={onClick}>
      <SendButtonViewer>
        <FontAwesome name="send" size={20} />
      </SendButtonViewer>
    </TouchableOpacity>
  );
};

export const onSend = ({
  iplayer,
  clickChat,
  chatUseInput,
  createMessageMutation,
  setImessage,
}) => {
  try {
    if (!chatUseInput.value) return;

    chatUseInput.reFresh();
    createMessageMutation({
      variables: {
        chatId: clickChat,
        playerId: iplayer,
        text: chatUseInput.value,
        type: 'normal',
      },
    });
    setImessage({
      id: new Date().toISOString(),
      from: { id: iplayer },
      text: chatUseInput.value,
      type: 'normal',
      createdAt: new Date().toISOString(),
    });
  } catch (e) {
    alert(e);
  }
};
