import React from 'react';
import {useMutation} from 'react-apollo-hooks';

import {CREATE_MESSAGE} from '../Share/Gql';
import {useInput, ChatInput} from './Input';
import {SendButton, onSend} from './Button';
import {Viewer} from './Resource/Style/StMain';

const Writer = ({iplayer, clickChat, setImessage}) => {
  console.log('DEV:: Chatroom::Writer');

  const [createMessageMutation] = useMutation(CREATE_MESSAGE);
  const chatUseInput = useInput('');

  const onSendClick = () => {
    onSend({
      iplayer,
      clickChat,
      chatUseInput,
      createMessageMutation,
      setImessage,
    });
  };

  return (
    <Viewer>
      <ChatInput {...chatUseInput} />
      <SendButton onClick={onSendClick} />
    </Viewer>
  );
};

export default Writer;
