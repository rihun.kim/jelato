import React, {useState} from 'react';
import {ChatInputViewer} from './Resource/Style/StInput';

export const useInput = prefill => {
  const [value, setValue] = useState(prefill);

  const onChange = text => {
    setValue(text);
  };
  const reFresh = () => {
    setValue('');
  };

  return {onChange, reFresh, value};
};

export const ChatInput = ({onChange, value}) => (
  <ChatInputViewer
    autoCapitalize={'none'}
    autoCorrect={false}
    multiline={true}
    onChangeText={onChange}
    onEndEditing={() => null}
    value={value}
  />
);
