import styled from 'styled-components';

export const SendButtonViewer = styled.View`
  width: 65px;
  height: 50px;

  flex-direction: column;
  align-items: center;
  justify-content: center;

  background-color: #fff977;
`;
