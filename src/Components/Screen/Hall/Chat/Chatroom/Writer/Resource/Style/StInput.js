import styled from 'styled-components';

export const ChatInputViewer = styled.TextInput`
  flex: 1;
  height: 40px;

  border: 0;
  background-color: #ffffff;
  font-size: 14px;
  padding: 0 15px 0 15px;
  text-align: left;
`;
