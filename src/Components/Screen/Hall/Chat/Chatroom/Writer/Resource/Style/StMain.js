import styled from 'styled-components';

export const Viewer = styled.View`
  width: 100%;
  height: 50px;

  background-color: #ffffff;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
