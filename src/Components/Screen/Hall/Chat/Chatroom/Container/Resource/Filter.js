import {convertChatTime} from './Time';

export const convertMessages = messages => {
  console.log('필터링');
  let previousMessageTime, currentMessageTime;
  let convertedMessages = [];

  for (const message of messages) {
    currentMessageTime = message.createdAt.substr(0, 10);

    if (previousMessageTime == currentMessageTime)
      convertedMessages.push(message);
    else {
      previousMessageTime = currentMessageTime;
      convertedMessages.push(
        {
          id: currentMessageTime,
          createdAt: convertChatTime(message.createdAt),
          type: 'time',
        },
        message,
      );
    }
  }

  return convertedMessages;
};
