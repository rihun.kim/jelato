export const convertChatTime = date => {
  const time = new Date(date);
  const week = [
    '일요일',
    '월요일',
    '화요일',
    '수요일',
    '목요일',
    '금요일',
    '토요일',
  ];
  const convertedTime =
    '' +
    time.getFullYear() +
    '년 ' +
    (time.getMonth() + 1) +
    '월 ' +
    time.getDate() +
    '일 ' +
    week[time.getDay()];

  return convertedTime;
};
