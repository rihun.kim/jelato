import React, {useState} from 'react';

import Reader from '../Reader/Main';
import Writer from '../Writer/Main';

const Render = ({iplayer, clickChat, filterMessages}) => {
  console.log('DEV:: Chatroom::Render');

  const [imessage, setImessage] = useState();
  // imessage 는 채팅방이 생기면 다시 새로 생김

  // 남의 메시지는 realtimeMessages 가 fetcher 에 의해서 갱신되므로
  // 리프레시 되면, 레퍼런스로 달아놓은 messages 들이 갱신되지 않을까? 예

  return (
    <>
      <Reader iplayer={iplayer} messages={filterMessages} imessage={imessage} />
      <Writer
        iplayer={iplayer}
        clickChat={clickChat}
        setImessage={setImessage}
      />
    </>
  );
};

export default Render;
