import React from 'react';

import {convertMessages} from './Resource/Filter';
import Render from './Render';

const Main = ({route, navigation}) => {
  console.log('DEV:: Chatroom');
  const {iplayer, clickChat, title, messages} = route.params;

  let filterMessages = [...convertMessages(messages)];

  return (
    <Render
      iplayer={iplayer}
      clickChat={clickChat}
      title={title}
      filterMessages={filterMessages}
    />
  );
};

export default Main;
