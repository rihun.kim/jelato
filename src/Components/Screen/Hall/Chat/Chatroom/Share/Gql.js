import {gql} from 'apollo-boost';

export const CREATE_MESSAGE = gql`
  mutation createMessage(
    $chatId: String!
    $playerId: String!
    $text: String!
    $type: String
  ) {
    createMessage(
      chatId: $chatId
      playerId: $playerId
      text: $text
      type: $type
    ) {
      id
    }
  }
`;
