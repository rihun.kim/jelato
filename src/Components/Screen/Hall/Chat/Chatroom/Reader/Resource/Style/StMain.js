import styled from 'styled-components';

import {CHATROOM_BACKGROUND} from '../../../Share/Constants';

export const Viewer = styled.View`
  flex: 1;

  background-color: ${CHATROOM_BACKGROUND};
  padding: 10px 0 10px 0;
`;
