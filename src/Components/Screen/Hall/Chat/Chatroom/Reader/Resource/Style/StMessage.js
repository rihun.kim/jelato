import styled from 'styled-components';

export const Official = {};
Official.Wrapper = styled.View`
  margin: 0 auto 10px auto;
`;

Official.Text = styled.Text`
  color: #ececec;
  font-size: 11px;
`;

export const Normal = {I: {}, Other: {}};
Normal.I.Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;

  margin: 0 15px 10px 0;
`;

Normal.I.Text = styled.Text`
  background-color: #fff977;
  border-radius: 5px;
  font-size: 14px;
  padding: 8px 8px 8px 8px;
`;

Normal.Other.Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  margin: 0 0 10px 15px;
`;

Normal.Other.Text = styled.Text`
  background-color: #ffffff;
  border-radius: 5px;
  font-size: 14px;
  padding: 8px 8px 8px 8px;
`;

export const Time = {};
Time.Wrapper = styled.View`
  margin: 0 auto 10px auto;
`;

Time.Text = styled.Text`
  color: #ececec;
  font-size: 11px;
`;
