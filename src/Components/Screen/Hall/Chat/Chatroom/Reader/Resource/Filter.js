import {convertChatTime} from './Time';

export const pushMessage = (messages, imessage) => {
  let lastMessageTime = messages[messages.length - 1].createdAt.substr(0, 10);
  let imessageTime = imessage.createdAt.substr(0, 10);

  if (lastMessageTime == imessageTime) {
    messages.push(imessage);
  } else {
    messages.push(
      {
        id: imessageTime,
        createdAt: convertChatTime(imessage.createdAt),
        type: 'time',
      },
      imessage,
    );
  }
};
