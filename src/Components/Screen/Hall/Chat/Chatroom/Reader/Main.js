import React, {useContext, useRef, useMemo, useEffect} from 'react';
import {FlatList} from 'react-native-gesture-handler';

import {OfficialMessage, NormalMessage, TimeMessage} from './Message';
import {pushMessage} from './Resource/Filter';
import {Viewer} from './Resource/Style/StMain';
import {ChatContext} from '../../../../../Selector/Context';

const Reader = ({iplayer, messages, imessage}) => {
  console.log('DEV:: Chatroom::Reader');

  const {realtimeMessages} = useContext(ChatContext);

  const isFirstRun = useRef(true);

  // 마운트되고 딱 한번만 수행
  useEffect(() => {
    if (isFirstRun.current) {
      console.log('useEffect');
      isFirstRun.current = false;
      return;
    }
  }, []);

  console.log(isFirstRun);

  // 이미 만들어진 것들은 다시 만들지 않도록
  // 바뀐 부분만 filtering 하기
  // 뒤에서부터 만들면서 바로 break
  // writer 에서 푸시하는 것을 Context 로 받고
  // 자신의 메시지에 추가시키고
  // 이후에 fetcher 에서 오는 것을 메시지에서 수정시킴

  // let filteredMessages;
  // useMemo(() => {
  //   if (messages) {
  //     filteredMessages = [...convertMessages(messages)];
  //     console.log(filteredMessages);
  //   }
  // }, [messages]);

  // 상황 realtimeMessages 가 발생하면, 방에 있을 경우 messages 에 넣는다.
  // 그런 다음 다시 나갔다가 들어오면, 이미 Fetcher 에서 넣은 상태인 messages 에 realtimeMessages 을 또 넣는다.
  // 이렇게 되서, 중복 메시지가 발생한다.
  // 어떻게 막을 수 있을까?
  // 방에 들어가면, realtimeMessages가 아까것으로 되어있는 채로 시작하기에
  // 다시 바로 넣는 것이다.
  // 원하는 상태는 채팅방에 있을때, 새 메시지를 넣어주고
  // 다시 나갔다가 들어왔을 때, realtimeMessages 가 비어있길 바란다.
  // 그래야 이전 꺼를 다시 넣지 않는다.
  // 방에 다시 들어올때, realtimeMessages 가 어떻게 하면 비어있게 할 수 있을까?

  useMemo(() => {
    if (!isFirstRun.current && realtimeMessages) {
      console.log('hi');
      if (iplayer != realtimeMessages.subscribeMessages.from.id) {
        // console.log(iplayer, realtimeMessages.subscribeMessages.from.id);
        pushMessage(messages, realtimeMessages.subscribeMessages);
      }
    }
  }, [realtimeMessages]);

  // console.log(messages);
  // console.log(realtimeMessages);

  useMemo(() => {
    if (!isFirstRun.current && imessage) {
      // 시나리오
      // 1. 방 생성 후, 바로 메시지를 쓰는 경우
      // 2. 방 생성 후, 한 참 뒤에 메시지를 쓰는 경우
      // 3. 방 생성 후, 남이 메시지를 쓴 다음, 메시지를 쓰는 경우
      // convertMessage 제작시
      // 맨 마지막의 메시지와 imessage 을 비교해서 제작
      // console.log('imessage filter', imessage);
      // fikteredMessages 에 넣기
      pushMessage(messages, imessage);

      // filteredMessages.push(
      //   convertMessage(messages[messages.length - 1], imessage)[0],
      // );
      // console.log(filteredMessages);
      // console.log(...convertMessage(messages[messages.length - 1], imessage));
      // console.log(messages);
      // messages.push(imessage);
    }
  }, [imessage]);

  // let flatlistRef;

  return (
    <Viewer>
      <FlatList
        // ref={ref => (flatlistRef = ref)}
        data={messages}
        // initialScrollIndex={filteredMessages.length - 1}
        // onContentSizeChange={() => flatlistRef.scrollToEnd({animated: false})}
        renderItem={({item}) => {
          if (item.type == 'official') return <OfficialMessage item={item} />;
          else if (item.type == 'normal')
            return <NormalMessage item={item} iplayer={iplayer} />;
          else if (item.type == 'time') return <TimeMessage item={item} />;
        }}
      />
    </Viewer>
  );
};

export default Reader;
