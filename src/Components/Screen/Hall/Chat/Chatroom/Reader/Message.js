import React from 'react';

import {Official, Normal, Time} from './Resource/Style/StMessage';

export const OfficialMessage = ({item}) => {
  return (
    <Official.Wrapper>
      <Official.Text>{item.text}</Official.Text>
    </Official.Wrapper>
  );
};

export const NormalMessage = ({item, iplayer}) => {
  if (item.from.id == iplayer) {
    return (
      <Normal.I.Wrapper key={item.id}>
        <Normal.I.Text>{item.text}</Normal.I.Text>
      </Normal.I.Wrapper>
    );
  } else {
    return (
      <Normal.Other.Wrapper key={item.id}>
        <Normal.Other.Text>{item.text}</Normal.Other.Text>
      </Normal.Other.Wrapper>
    );
  }
};

export const TimeMessage = ({item}) => {
  return (
    <Time.Wrapper>
      <Time.Text>{item.createdAt}</Time.Text>
    </Time.Wrapper>
  );
};
