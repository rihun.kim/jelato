import React, {useContext, useState, useMemo} from 'react';

import Reader from '../Reader/Main';
import {ChatContext} from '../../../../../Selector/Context';
import {lastMessagesCache} from './Resource/Cache';
import {getIplayerFromChatPlayers} from './Resource/Filter';

const Main = ({navigation}) => {
  console.log('DEV:: Chatlist');

  const [chatIplayers, setChatIplayers] = useState();
  let {iplayers, remoteChats, realtimeMessages} = useContext(ChatContext);

  useMemo(() => {
    if (lastMessagesCache.size != 0) {
      lastMessagesCache[realtimeMessages.chat.id].messages = cloneDeep(
        realtimeMessages,
      );
    } else {
      for (let chat of remoteChats.readChats) {
        lastMessagesCache[chat.id] = {
          title: chat.title,
          players: chat.players,
          messages: chat.messages.slice(-1)[0],
        };
      }
    }
  }, [realtimeMessages]);

  // Chat 이 추가되거나 삭제될 경우 업데이트 행해야함
  useMemo(() => {
    setChatIplayers({
      iplayers: remoteChats.readChats.map(({players}) =>
        getIplayerFromChatPlayers(players, iplayers),
      ),
    });
  }, [remoteChats]);

  return (
    <Reader
      iplayers={iplayers}
      remoteChats={remoteChats}
      chatIplayers={chatIplayers}
      lastMessagesCache={lastMessagesCache}
      navigation={navigation}
    />
  );
};

export default Main;
