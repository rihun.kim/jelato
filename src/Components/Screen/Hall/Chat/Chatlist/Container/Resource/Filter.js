export const getIplayerFromChatPlayers = (players, iplayers) => {
  for (const player of players)
    if (iplayers.includes(player.id)) return player.id;
};
