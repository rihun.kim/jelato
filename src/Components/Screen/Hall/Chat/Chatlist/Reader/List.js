import React from 'react';

import {getCommaList} from './Resource/Filter';
import {convertReceiveTime} from './Resource/Time';
import * as Style from './Resource/Style/StList';

const List = ({item, index, chatIplayers, onPress}) => {
  return (
    <Style.Wrapper onPress={onPress}>
      <Style.Header>
        <Style.Title>
          {item.title === 'none'
            ? getCommaList(item.players, chatIplayers[index])
            : item.title}
        </Style.Title>
        <Style.Time>{convertReceiveTime(item.messages.createdAt)}</Style.Time>
      </Style.Header>
      <Style.Message>{item.messages.text}</Style.Message>
    </Style.Wrapper>
  );
};

export default List;
