import {convertReceiveTime} from './Time';

export const getIplayerFromChatPlayers = (players, iplayers) => {
  for (const player of players)
    if (iplayers.includes(player.id)) return player.id;
};

export const getCommaList = (players, iplayer) => {
  let commaList = '';

  for (const player of players) {
    if (player.id != iplayer) commaList += player.name + ', ';
    if (commaList.length > 12) break;
  }

  return commaList.slice(0, commaList.length - 2);
};

export const getChatroomList = (chats, iplayers) => {
  console.log('func');

  let chatroomList = [];

  let chatIplayers = chats.map(({players}) =>
    getIplayerFromChatPlayers(players, iplayers),
  );

  for (const [i, chat] of chats.entries()) {
    let item = {title: '', time: '', lastMessage: ''};

    if (chat.title == 'none')
      item.title = getCommaList(chat.players, chatIplayers[i]);
    else item.title = chat.title;

    if (chat.messages.length == 0)
      item.time = convertReceiveTime(chat.messages.slice(-1)[0].createdAt);
    else item.time = convertReceiveTime(chat.createdAt);

    item.lastMessage = chat.messages.slice(-1)[0].text;
    chatroomList.push(item);
  }

  console.log(chatroomList);

  return chatroomList;
};
