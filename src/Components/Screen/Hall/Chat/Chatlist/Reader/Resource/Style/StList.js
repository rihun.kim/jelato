import styled from 'styled-components';

// TO FIX ellipse 넣기
export const Wrapper = styled.TouchableOpacity`
  width: 100%;
  height: 70px;

  flex-direction: column;

  background-color: #ffffff;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.Text`
  width: 70%;

  color: #000000;
  font-size: 15px;
  margin: 10px 10px 0 15px;
`;

export const Time = styled.Text`
  color: #a0a0a0;
  font-size: 10px;
  margin: 10px 20px 0 0;
`;

export const Message = styled.Text`
  width: 100%;

  color: #a0a0a0;
  font-size: 13px;
  padding: 10px 10px 10px 15px;
`;
