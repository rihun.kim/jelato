//
export const convertReceiveTime = time => {
  const rTime = new Date(time);
  const rYear = rTime.getFullYear(),
    rMonth = rTime.getMonth(),
    rDate = rTime.getDate(),
    rHour = rTime.getHours(),
    rMin = rTime.getMinutes();

  const cTime = new Date();
  const cYear = cTime.getFullYear(),
    cMonth = cTime.getMonth(),
    cDate = cTime.getDate();

  if (rYear === cYear)
    if (rMonth === cMonth)
      if (rDate === cDate)
        return rHour <= 12
          ? '오전 ' + rHour + ':' + rMin
          : '오후 ' + (rHour - 12) + ':' + rMin;
      else if (rDate + 1 === cDate) return '어제';
      else return rMonth + 1 + '월 ' + rDate + '일 ';
    else return rYear + '.' + (rMonth + 1) + '.' + rDate;
};
