import React, {useEffect, useMemo} from 'react';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';

import List from './List';
import {getIplayerFromChatPlayers, getChatroomList} from './Resource/Filter';
import {Viewer} from './Resource/Style/StMain';
import {ROUTE_CHATROOM_NAME} from '../../../../../Share/Constant';
import {cloneDeep} from 'lodash';

const Main = ({
  iplayers,
  chatIplayers,
  remoteChats,
  lastMessagesCache,
  navigation,
}) => {
  console.log('DEV:: Chatlist::Reader');

  const onNavClick = index => {
    const iplayer = chatIplayers.iplayers[index];
    const clickChat = remoteChats.readChats[index].id;
    const title = remoteChats.readChats[index].title;

    // clone 뜨지 말고, 채팅방에서 레퍼런스에 내용을 달고,
    // 메시지가 수신되면, fetcher 에서 갈아끼는 방식으로
    let messages = remoteChats.readChats[index].messages;

    navigation.navigate(ROUTE_CHATROOM_NAME, {
      iplayer,
      clickChat,
      title,
      messages,
    });
  };

  //TODO keyExtractor 제대로 주기
  return (
    <Viewer>
      <FlatList
        keyExtractor={item => item.messages.id}
        data={Object.values(lastMessagesCache)}
        renderItem={({item, index}) => (
          <List
            item={item}
            index={index}
            chatIplayers={chatIplayers.iplayers}
            onPress={() => onNavClick(index)}
          />
        )}
      />
    </Viewer>
  );
};

export default Main;
