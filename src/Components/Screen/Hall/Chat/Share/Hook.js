import {useQuery, useSubscription} from 'react-apollo-hooks';

import {READ_CHATS, SUBSCRIBE_MESSAGES} from './Gql';

export const UseQueryChats = iplayerIds => {
  const {loading, data} = useQuery(READ_CHATS, {
    variables: {playerIds: iplayerIds},
    fetchPolicy: 'no-cache',
  });

  if (loading || data === undefined) return false;
  return data;
};

export const UseSubsMessages = iplayerIds => {
  const {loading, data} = useSubscription(SUBSCRIBE_MESSAGES, {
    variables: {playerIds: iplayerIds},
  });

  if (loading || data === undefined) return false;
  return data;
};
