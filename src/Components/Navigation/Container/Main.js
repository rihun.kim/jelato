import React from 'react';

import { LobbyRoute, HallRoute } from './Route';
import { isLogin } from '../../../Auth/Context';

const Main = () => {
  console.log('DEV:: Navigation');

  // splashscreen 닫는 부분도 여기 이후부터 고려해봐야함

  return isLogin() ? <HallRoute /> : <LobbyRoute />;
};

export default Main;
