import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Lobby from '../../Screen/Lobby/Container/Main';
import { ROUTE_LOBBY_NAME } from '../../Share/Constant';

const Stack = createStackNavigator();
const Main = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name={ROUTE_LOBBY_NAME} component={Lobby} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Main;
