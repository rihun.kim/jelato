import {gql} from 'apollo-boost';

export const READ_OWNPLAYERS = gql`
  query {
    readOwnplayers {
      id

      name
      position
    }
  }
`;
