import {useQuery} from 'react-apollo-hooks';

import {READ_OWNPLAYERS} from './Gql';

export const UseQueryIplayers = () => {
  const {loading, data} = useQuery(READ_OWNPLAYERS, {
    fetchPolicy: 'no-cache',
  });

  if (loading || data === undefined) return false;
  return data;
};
