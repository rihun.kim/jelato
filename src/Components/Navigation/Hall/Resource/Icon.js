import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const ChatIcon = ({color}) => (
  <Ionicons name="md-chatbubble" size={22} color={color} />
);

export const PlaygroundIcon = ({color}) => (
  <MaterialCommunityIcons name="cards" size={25} color={color} />
);

export const SettingIcon = ({color}) => (
  <Ionicons name="settings-sharp" size={22} color={color} />
);
