import {
  ROUTE_CHATLIST_NAME,
  ROUTE_CHATROOM_NAME,
  ROUTE_PLAYGROUND_NAME,
  ROUTE_SETTING_NAME,
} from '../../../Share/Constant';

export const getHeaderTitleFromRoute = route => {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : route.params?.screen || ROUTE_CHATLIST_NAME;

  if (routeName == ROUTE_CHATLIST_NAME) return 'Chat';
  else if (routeName == ROUTE_PLAYGROUND_NAME) return 'Playground';
  else if (routeName == ROUTE_SETTING_NAME) return 'Setting';
  else if (routeName == ROUTE_CHATROOM_NAME) return 'Chatroom';
};
