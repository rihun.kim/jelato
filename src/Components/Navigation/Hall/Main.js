import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Selector from '../../Selector/Container/Main';
import { ROUTE_HALL_NAME, ROUTE_CHATROOM_NAME } from '../../Share/Constant';
import { TabRoute, ChatroomRoute } from './Route';
import { getHeaderTitleFromRoute } from './Resource/Filter';

const Stack = createStackNavigator();
const Main = () => {
  console.log('DEV:: Hall');

  const main = (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={ROUTE_HALL_NAME}
          component={TabRoute}
          options={({ route }) => ({
            headerTitle: getHeaderTitleFromRoute(route),
          })}
        />
        <Stack.Screen
          name={ROUTE_CHATROOM_NAME}
          component={ChatroomRoute}
          options={({ route }) => ({
            headerLeft: null,
            // headerTitle: getHeaderTitle(route),
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );

  return <Selector main={main} />;
};

export default Main;
