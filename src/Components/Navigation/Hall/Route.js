import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Chatroom from '../../Screen/Hall/Chat/Chatroom/Container/Main';
import Chatlist from '../../Screen/Hall/Chat/Chatlist/Container/Main';
import Playground from '../../Screen/Hall/Playground/Arbiter';
import Setting from '../../Screen/Hall/Setting/Arbiter';
import {
  ROUTE_CHATLIST_NAME,
  ROUTE_PLAYGROUND_NAME,
  ROUTE_SETTING_NAME,
  TAB_ACTIVE_COLOR,
  TAB_INACTIVE_COLOR,
} from '../../Share/Constant';
import { ChatIcon, PlaygroundIcon, SettingIcon } from './Resource/Icon';

const BottomTab = createBottomTabNavigator();
export const TabRoute = () => {
  return (
    <BottomTab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: TAB_ACTIVE_COLOR,
        inactiveTintColor: TAB_INACTIVE_COLOR,
      }}>
      <BottomTab.Screen
        name={ROUTE_CHATLIST_NAME}
        component={Chatlist}
        options={{
          tabBarIcon: ({ color }) => <ChatIcon color={color} />,
        }}
      />
      <BottomTab.Screen
        name={ROUTE_PLAYGROUND_NAME}
        component={Playground}
        options={{
          tabBarIcon: ({ color }) => <PlaygroundIcon color={color} />,
        }}
      />
      <BottomTab.Screen
        name={ROUTE_SETTING_NAME}
        component={Setting}
        options={{
          tabBarIcon: ({ color }) => <SettingIcon color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
};

export const ChatroomRoute = ({ route, navigation }) => {
  return <Chatroom route={route} navigation={navigation} />;
};
