//
export const ROUTE_LOBBY_NAME = 'lobby';
export const ROUTE_HALL_NAME = 'hall';
export const ROUTE_CHAT_NAME = 'chat';
export const ROUTE_CHATLIST_NAME = 'chatlist';
export const ROUTE_CHATROOM_NAME = 'chatroom';
export const ROUTE_PLAYGROUND_NAME = 'playground';
export const ROUTE_SETTING_NAME = 'setting';

//
export const TAB_ACTIVE_COLOR = 'black';
export const TAB_INACTIVE_COLOR = 'gray';
