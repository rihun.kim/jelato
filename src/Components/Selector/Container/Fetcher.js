import React, { useState, useEffect, useMemo } from 'react';
import { cloneDeep } from 'lodash';

import Render from './Render';
import { ChatContext } from '../Context';
import { UseQueryChats, UseSubsMessages } from '../Hook';

const Fetcher = ({ main, remoteIplayers }) => {
  console.log('DEV:: Fetcher');

  let iplayers = remoteIplayers.readOwnplayers.map(player => player.id);
  let remoteChats = UseQueryChats(iplayers);
  let realtimeMessages = UseSubsMessages(iplayers);

  useMemo(() => {
    if (realtimeMessages) {
      for (const chat of remoteChats.readChats) {
        if (chat.id == realtimeMessages.subscribeMessages.chat.id) {
          // if (
          //   realtimeMessages.subscribeMessages.type == 'official' ||
          //   !iplayers.includes(realtimeMessages.subscribeMessages.from.id)
          // ) {
          chat.messages.push(cloneDeep(realtimeMessages.subscribeMessages));
          break;
          // }
        }
      }
    }
  }, [realtimeMessages]);

  if (!remoteChats) return <></>;
  return (
    <ChatContext.Provider
      value={{
        iplayers: iplayers,
        remoteChats: remoteChats,
        realtimeMessages: realtimeMessages,
      }}>
      <Render main={main} />
    </ChatContext.Provider>
  );
};

export default Fetcher;
