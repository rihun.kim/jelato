import React from 'react';

import Fetcher from './Fetcher';
import { UseQueryIplayers } from '../Hook';

const Main = ({ main }) => {
  let remoteIplayers = UseQueryIplayers();

  if (!remoteIplayers) return <></>;
  return <Fetcher main={main} remoteIplayers={remoteIplayers} />;
};

export default Main;
