import {gql} from 'apollo-boost';

export const READ_OWNPLAYERS = gql`
  query {
    readOwnplayers {
      id

      name
      position
    }
  }
`;

export const READ_CHATS = gql`
  query readChats($playerIds: [String]) {
    readChats(playerIds: $playerIds) {
      id

      title
      count
      owner
      players {
        id
        name
      }
      messages {
        id
        from {
          id
          name
        }
        text
        type
        createdAt
      }
      createdAt
    }
  }
`;

export const SUBSCRIBE_MESSAGES = gql`
  subscription subscribeMessages($playerId: String, $playerIds: [String]) {
    subscribeMessages(playerId: $playerId, playerIds: $playerIds) {
      id

      chat {
        id
      }
      from {
        id
        name
      }
      text
      type
      createdAt
    }
  }
`;
