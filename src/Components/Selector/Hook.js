import {useQuery, useSubscription} from 'react-apollo-hooks';

import {READ_CHATS, READ_OWNPLAYERS, SUBSCRIBE_MESSAGES} from './Gql';

// TODO skip 기능 추가
export const UseQueryIplayers = () => {
  const {loading, data} = useQuery(READ_OWNPLAYERS, {
    fetchPolicy: 'no-cache',
  });

  if (loading || data === undefined) return false;
  return data;
};

// READ_CHATS 은 챗의 변동내용을 인지해서 새로 가져오는 것이지
// 메시지 부분의 새로운 내용이 있다고 해서 업데이트 하진 않음
export const UseQueryChats = iplayerIds => {
  const {loading, data} = useQuery(READ_CHATS, {
    variables: {playerIds: iplayerIds},
    fetchPolicy: 'no-cache',
  });

  if (loading || data === undefined) return false;
  return data;
};

export const UseSubsMessages = iplayerIds => {
  const {loading, data} = useSubscription(SUBSCRIBE_MESSAGES, {
    variables: {playerIds: iplayerIds},
  });

  if (loading || data === undefined) return false;
  return data;
};
