import { AppRegistry } from 'react-native';

import { name } from './app.json';
import App from './src/App/Main';

AppRegistry.registerComponent(name, () => App);
